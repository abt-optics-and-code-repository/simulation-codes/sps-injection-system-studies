"""
Meta-solver for optimisation of SPS injection for all optics: SFTPRO, Q20 and Q26. Optimisation environments for the respective 
optics are created, then the meta-optimiser (wrapper) find the ideal number of MKP tanks and number of modules, while keeping the 
needed kick and magnetic field + integrated magnetic field as low as possible 
"""

import numpy as np
#matplotlib.use('Agg')  #to save plots remotely from ssh
import matplotlib.pyplot as plt

#For the optimisation part
from pymoo.factory import get_algorithm, get_crossover, get_mutation, get_sampling
from pymoo.optimize import minimize
from pymoo.core.problem import ElementwiseProblem

# Some initial settings 
fig_flag = 1  #set to one to save generated figures
extra_str = "_no_offset"  #add any extra option to name string, e.g. offset 

#Import high-level and low-levels environments
import sps_injection_opt_env_global


#Initiate global environment 
glob_env = sps_injection_opt_env_global.OptEnv_global()


# ---------------------------- META-OPTIMISATION OF INJECTION WITH PYMOO ----------------------------
class MetaProblem(ElementwiseProblem, sps_injection_opt_env_global.OptEnv_global):  

    def __init__(self, **kwargs):
        super().__init__(n_var=2, n_obj=1, n_constr=0, xl=np.array([1,1]), xu=np.array([4,5]), type_var=int)
        self.env = sps_injection_opt_env_global.OptEnv_global()  #initiate the global environment 
                
    def _evaluate(self, x, out, *args, **kwargs):
        
        f = self.env.step([x[0], x[1]])
        out["F"] = [f]          


method = get_algorithm("ga",
                       pop_size=10,
                       sampling=get_sampling("int_random"),
                       crossover=get_crossover("int_sbx", prob=1.0, eta=3.0),
                       mutation=get_mutation("int_pm", eta=3.0),
                       eliminate_duplicates=True,
                       )

#Perform genetic algorithm optimisation on the higher level
res = minimize(MetaProblem(),
               method,
               termination=('n_gen', 15),
               seed=1,
               save_history=False
               )

print("Best solution found: {}".format(res.X))
print("Function value: %s" % res.F)
print("Constraint violation: %s" % res.CV)
n_modules = res.X[0]

#Perform last meta-optimisation with best position in n-vector space 
glob_env.step(res.X) 
glob_env.optimise_kicks(res.X)  #also for other optics


# --------------------- PLOT ENVELOPE AND APERTURE IF NEEDED, TO SEE PERFORMANCE -------------------------------- 
fig1 = plt.figure(figsize=(10,7))
glob_env.plot_injection_q20(fig1)
if fig_flag == 1:
    fig1.savefig('plots/sps_injection_x_meta_{}_{}_n{}{}.png'.format('q20', glob_env.mkp_str, n_modules, extra_str), dpi=400)

fig2 = plt.figure(figsize=(10,7))
glob_env.plot_injection_q26(fig2)
if fig_flag == 1:
    fig2.savefig('plots/sps_injection_x_meta_{}_{}_n{}{}.png'.format('q26', glob_env.mkp_str, n_modules, extra_str), dpi=400)

fig3 = plt.figure(figsize=(10,7))
glob_env.plot_injection_sftpro(fig3)
if fig_flag == 1:
    fig3.savefig('plots/sps_injection_x_meta_{}_{}_n{}{}.png'.format('sftpro', glob_env.mkp_str, n_modules, extra_str), dpi=400)

fig4 = plt.figure(figsize=(10,7))
glob_env.plot_injection_q20_no_mkp(fig4)
if fig_flag == 1:
    fig4.savefig('plots/sps_injection_x_meta_{}_{}_n{}_no_mkp{}.png'.format('q20', glob_env.mkp_str, n_modules, extra_str), dpi=400)

fig5 = plt.figure(figsize=(10,7))
glob_env.plot_injection_q26_no_mkp(fig5)
if fig_flag == 1:
    fig5.savefig('plots/sps_injection_x_meta_{}_{}_n{}_no_mkp{}.png'.format('q26', glob_env.mkp_str, n_modules, extra_str), dpi=400)

fig6 = plt.figure(figsize=(10,7))
glob_env.plot_injection_sftpro_no_mkp(fig6)
if fig_flag == 1:
    fig6.savefig('plots/sps_injection_x_meta_{}_{}_n{}_no_mkp{}.png'.format('sftpro', glob_env.mkp_str, n_modules, extra_str), dpi=400)