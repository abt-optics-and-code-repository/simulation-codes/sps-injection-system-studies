"""
Script to generate initial conditions for a given range of MKP modules and MKP vacuum tanks for all optics, to facilitate the 
meta-optimisation with fair starting values 
"""

import numpy as np
import pandas as pd
import scipy as sp
import matplotlib.pyplot as plt
import pathlib

#Import accelerator physics library with functions
import acc_phy_lib_elias 

#Import high-level environment
import sps_injection_opt_env_low_level

#Initialize parameters for the initial condition sweep
msi_blade_dx = 0.02  #thickness of second half of MSI blade if default one is not used  
solver = 'Nelder-Mead'
tank_nr = [1, 2, 3, 4, 5]  #possible MKP vacuum tank number to iterate over 
module_nr = [1, 2, 3, 4]   #possible MKP module number per tank to iterate over 


#Perform a general sweeps of mkp tank number and mkp module number, load previous optimisation results if needed 
for i in tank_nr:
    for j in module_nr:
            
        mkp_nr = i  #number of MKP vacuum tanks
        n = j  #number of magnet modules per vacuum tank 
        
        print("\n------------- Optimising for {} MKP modules in {} tanks -------------\n".format(n, mkp_nr))
        
        #Load the environments
        env_sftpro = sps_injection_opt_env_low_level.OptEnv('sftpro', n, mkp_nr)
        env_q20 = sps_injection_opt_env_low_level.OptEnv('q20', n, mkp_nr)
        env_q26 = sps_injection_opt_env_low_level.OptEnv('q26', n, mkp_nr)
        
        # ---------------------------- OPTIMISATION PART OF EACH ENVIRONMENT -----------------------------------------------
        #Fill in the string value for one or several modules
        if mkp_nr == 1:
            mkp_str = 'single_module'
        elif mkp_nr == 2:
            mkp_str = 'double_module'
        elif mkp_nr == 3:
            mkp_str = 'triple_module'
        else:
            mkp_str = '{}_module'.format(mkp_nr)
                                         
        # ---------------------------- Set initial and boundary conditions --------------------------
        #Calculate original values before optimising 
        mkp_org_kick = env_sftpro.madx.globals['kmkpa11931'] 
        mkp_org_l = env_sftpro.madx.sequence['short_injection_basic'].elements['mkp_module_1'].l #same length for all modules
        kmsi_original_kick = env_sftpro.madx.globals['kmsi']
        Bdl_tot_q20 = 3.17200000e-01  #Original total Bdl values for all MKPs, calculated from notebook "today_sps_injection_acceptance_q20"
        Bdl_tot_q26 = 3.17200000e-01
        Bdl_tot_sftpro = 2.14273280e-01
        
        #Minimum distance between MKPs
        s_min_mkp = mkp_org_l/2*n/5 + \
            mkp_org_l/2*n/5 + 0.3 #at least 300 mm between tanks such that vacuum valves can fit 
        
        # ------------------- Load the initial conditions -----------------------------------   
        #Starting initial conditions if no previous ICs exist
        x0 = np.zeros(4) 
        x0[0] = kmsi_original_kick   #original kmsi
        x0[1] = 1.4*mkp_org_kick   #original MKP kick   #for one MKP tank and low n, set about 1.5
        x0[2] = 33.1      #MSI_1 first position
        x0[3] = 64.0      #MKP tank 1 position 
        for k in range(1, mkp_nr):
            x0 = np.append(x0, env_sftpro.s_min_mkp)  #MKP tank k w.r.t tank k-1
        
        #Load previous initial conditions, if they exist 
        try:
            x0_q20 = np.genfromtxt("IC_data/{}/{}_n{}_{}_optimiser_results.csv".format('q20', mkp_str, n, 'q20')) 
            print("Q20: Loading initial conditions from previous runs...")
        except OSError:
            x0_q20 = x0
        try:
            x0_q26 = np.genfromtxt("IC_data/{}/{}_n{}_{}_optimiser_results.csv".format('q26', mkp_str, n, 'q26')) 
            print("Q26: Loading initial conditions from previous runs...")
        except OSError:
            x0_q26 = x0
        try:
            x0_sftpro = np.genfromtxt("IC_data/{}/{}_n{}_{}_optimiser_results.csv".format('sftpro', mkp_str, n, 'sftpro')) #print("Loading initial conditions from previous runs...")
            print("SFTPRO: Loading initial conditions from previous runs...")
        except OSError:   
            x0_sftpro = x0
        
        #Bounds for the optimisation
        rel_diff = 0.9*abs(kmsi_original_kick)  #+/-90%
        kmsi_min = kmsi_original_kick - rel_diff
        kmsi_max = kmsi_original_kick + rel_diff
        bound1 = (kmsi_min, kmsi_max)  #kmsi
        bound2 = (0, 15*mkp_org_kick)     #MKP kick min and max
        bound3 = (33.55, 56.0)      #MSI_1 first position
        bound4 = (env_q20.first_mkp_min_s, 82.5)      #MKP tank 1 position
        bounds_mkp = (env_sftpro.s_min_mkp, 1.05*env_sftpro.s_min_mkp)      #MKP tank i position w.r.t MKP tank i-1 
        bounds = [bound1, bound2, bound3, bound4]
        for k in range(1, mkp_nr):
           bounds.append(bounds_mkp)  #MKP tank k w.r.t tank k-1
        
        #Optimize the values 
        res_q20 = sp.optimize.minimize(env_q20.step, x0_q20, method=solver, bounds=bounds, options={'disp': True, 'adaptive': True})
        res_q26 = sp.optimize.minimize(env_q26.step, x0_q26, method=solver, bounds=bounds, options={'disp': True, 'adaptive': True})
        res_sftpro = sp.optimize.minimize(env_sftpro.step, x0_sftpro, method=solver, bounds=bounds, options={'disp': True, 'adaptive': True}) 
        
        #Save these as initial conditions for further optimisations 
        pathlib.Path('IC_data/{}'.format('q20')).mkdir(parents=True, exist_ok=True)  #creates a new directory if it does not exist
        pathlib.Path('IC_data/{}'.format('q26')).mkdir(parents=True, exist_ok=True) 
        pathlib.Path('IC_data/{}'.format('sftpro')).mkdir(parents=True, exist_ok=True) 
        pd.DataFrame(res_q20.x).to_csv("IC_data/{}/{}_n{}_{}_optimiser_results.csv".format('q20', mkp_str, n, 'q20'), index=False, header=False)
        pd.DataFrame(res_q26.x).to_csv("IC_data/{}/{}_n{}_{}_optimiser_results.csv".format('q26', mkp_str, n, 'q26'), index=False, header=False)
        pd.DataFrame(res_sftpro.x).to_csv("IC_data/{}/{}_n{}_{}_optimiser_results.csv".format('sftpro', mkp_str, n, 'sftpro'), index=False, header=False)
        
        #Find the beam envelope for normal and dumped beam
        twiss_sftpro, twiss_no_mkp_sftpro, A_x_min_tot_sftpro, s_min_tot_sftpro, A_y_min_tot_sftpro, s_y_min_tot_sftpro = env_sftpro.return_twisses()
        twiss_q20, twiss_no_mkp_q20, A_x_min_tot_q20, s_min_tot_q20, A_y_min_tot_q20, s_y_min_tot_q20 = env_q20.return_twisses()
        twiss_q26, twiss_no_mkp_q26, A_x_min_tot_q26, s_min_tot_q26, A_y_min_tot_q26, s_y_min_tot_q26 = env_q26.return_twisses()
        
        BRHO_q20 = env_q20.madx.sequence['short_injection_basic'].beam.pc*3.3356
        BRHO_q26 = env_q26.madx.sequence['short_injection_basic'].beam.pc*3.3356
        BRHO_sftpro = env_sftpro.madx.sequence['short_injection_basic'].beam.pc*3.3356
        Bdl_tot_q20 =  (mkp_nr*res_q20.x[1])*BRHO_q20 #(Sum all kicks)*BRHO --> mkp_nr of equal MKPs
        Bdl_tot_q26 =  (mkp_nr*res_q26.x[1])*BRHO_q26
        Bdl_tot_sftpro =  (mkp_nr*res_sftpro.x[1])*BRHO_sftpro
        
        #Get neat aperture for SPS, and select index for the desired apertures
        new_pos_x_q20, aper_neat_x_q20 = acc_phy_lib_elias.get_apertures_real(twiss_q20)
        ind_q20 = (new_pos_x_q20 > twiss_q20.loc['msi_test_4'].s)
        offset_x_q20 = acc_phy_lib_elias.get_aper_offset_real(twiss_q20)
        new_pos_x_q26, aper_neat_x_q26 = acc_phy_lib_elias.get_apertures_real(twiss_q26)
        ind_q26 = (new_pos_x_q26 > twiss_q26.loc['msi_test_4'].s)
        offset_x_q26 = acc_phy_lib_elias.get_aper_offset_real(twiss_q26)
        new_pos_x_sftpro, aper_neat_x_sftpro = acc_phy_lib_elias.get_apertures_real(twiss_sftpro)
        ind_sftpro = (new_pos_x_sftpro > twiss_sftpro.loc['msi_test_4'].s)
        offset_x_sftpro = acc_phy_lib_elias.get_aper_offset_real(twiss_sftpro)
        
        print("\n","-"*65, "\n")


#UNCOMMENT IF PLOTTING IS DESIRED FOR SPECIFIC CASE
"""
# --------------------- PLOT ENVELOPE AND APERTURE IF NEEDED, TO SEE PERFORMANCE -------------------------------- 
fig1 = plt.figure(figsize=(10,7))
fig1.suptitle('SPS injection - Q20',fontsize=20)
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)  #can also set rotation
plt.yticks(fontsize=14)
plt.ylim(-0.1, 0.15)
plt.xlim(30, 92)
acc_phy_lib_elias.plot_envelope(twiss_q20, env_q20.sige, env_q20.ex, env_q20.ey, ax)
if env_q20.default_blade_flag == 0:
    acc_phy_lib_elias.plot_msi_blade(twiss_q20, 'msi_test_1', 'msi_test_4', ax, dx=msi_blade_dx)
else:
    acc_phy_lib_elias.plot_default_msi_blade(twiss_q20, ax, 'msi_test_1')
acc_phy_lib_elias.plot_aper_real(new_pos_x_q20[ind_q20], aper_neat_x_q20[ind_q20], ax, offset=offset_x_q20[ind_q20])
plt.text(31.5, 0.03, '$A_{{x, min}}= {:.2f}$'.format(A_x_min_tot_q20), fontsize=15, horizontalalignment='left')
plt.text(31.5, 0.015, 'at $s = {:.1f}$ m'.format(s_min_tot_q20), fontsize=15, horizontalalignment='left')
plt.text(31.5, -0.057, '$Bdl = {:>.4f}$ Tm'.format(Bdl_tot_q20), fontsize=15, horizontalalignment='left')
plt.text(31.5, -0.09, 'n = {}'.format(n), fontsize=15, horizontalalignment='left')

fig3 = plt.figure(figsize=(10,7))
fig3.suptitle('SPS injection - Q26',fontsize=20)
ax3 = fig3.add_subplot(1, 1, 1)  # create an axes object in the figure
ax3.yaxis.label.set_size(20)
ax3.xaxis.label.set_size(20)
plt.xticks(fontsize=14)  #can also set rotation
plt.yticks(fontsize=14)
plt.ylim(-0.1, 0.15)
plt.xlim(30, 92)
acc_phy_lib_elias.plot_envelope(twiss_q26, env_q26.sige, env_q26.ex, env_q26.ey, ax3)
if env_q26.default_blade_flag == 0:
    acc_phy_lib_elias.plot_msi_blade(twiss_q26, 'msi_test_1', 'msi_test_4', ax3, dx=msi_blade_dx)
else:
    acc_phy_lib_elias.plot_default_msi_blade(twiss_q26, ax3, 'msi_test_1')
acc_phy_lib_elias.plot_aper_real(new_pos_x_q26[ind_q26], aper_neat_x_q26[ind_q26], ax3, offset=offset_x_q26[ind_q26])
plt.text(31.5, 0.03, '$A_{{x, min}}= {:.2f}$'.format(A_x_min_tot_q26), fontsize=15, horizontalalignment='left')
plt.text(31.5, 0.015, 'at $s = {:.1f}$ m'.format(s_min_tot_q26), fontsize=15, horizontalalignment='left')
plt.text(31.5, -0.057, '$Bdl = {:>.4f}$ Tm'.format(Bdl_tot_q26), fontsize=15, horizontalalignment='left')
plt.text(31.5, -0.09, 'n = {}'.format(n), fontsize=15, horizontalalignment='left')

fig2 = plt.figure(figsize=(10,7))
fig2.suptitle('SPS injection - SFTPRO',fontsize=20)
ax2 = fig2.add_subplot(1, 1, 1)  # create an axes object in the figure
ax2.yaxis.label.set_size(20)
ax2.xaxis.label.set_size(20)
plt.xticks(fontsize=14)  #can also set rotation
plt.yticks(fontsize=14)
plt.ylim(-0.1, 0.15)
plt.xlim(30, 92)
acc_phy_lib_elias.plot_envelope(twiss_sftpro, env_sftpro.sige, env_sftpro.ex, env_sftpro.ey, ax2, nx=6, ny=4)
if env_sftpro.default_blade_flag == 0:
    acc_phy_lib_elias.plot_msi_blade(twiss_sftpro, 'msi_test_1', 'msi_test_4', ax2, dx=msi_blade_dx)
else:
    acc_phy_lib_elias.plot_default_msi_blade(twiss_sftpro, ax2, 'msi_test_1')
acc_phy_lib_elias.plot_aper_real(new_pos_x_sftpro[ind_sftpro], aper_neat_x_sftpro[ind_sftpro], ax2, offset=offset_x_sftpro[ind_sftpro])
plt.text(31.5, 0.03, '$A_{{x, min}}= {:.2f}$'.format(A_x_min_tot_sftpro), fontsize=15, horizontalalignment='left')
plt.text(31.5, 0.015, 'at $s = {:.1f}$ m'.format(s_min_tot_sftpro), fontsize=15, horizontalalignment='left')
plt.text(31.5, -0.057, '$Bdl = {:>.4f}$ Tm'.format(Bdl_tot_sftpro), fontsize=15, horizontalalignment='left')
plt.text(31.5, -0.09, 'n = {}'.format(n), fontsize=15, horizontalalignment='left')

fig5 = plt.figure(figsize=(10,7))
fig5.suptitle('SPS injection - Q20, dumped beam',fontsize=20)
ax5 = fig5.add_subplot(1, 1, 1)  # create an axes object in the figure
ax5.yaxis.label.set_size(20)
ax5.xaxis.label.set_size(20)
plt.xticks(fontsize=14)  #can also set rotation
plt.yticks(fontsize=14)
plt.ylim(-0.1, 0.15)
plt.xlim(30, 92)
acc_phy_lib_elias.plot_envelope(twiss_no_mkp_q20, env_q20.sige, env_q20.ex, env_q20.ey, ax5)
if env_q20.default_blade_flag == 0:
    acc_phy_lib_elias.plot_msi_blade(twiss_no_mkp_q20, 'msi_test_1', 'msi_test_4', ax5, dx=msi_blade_dx)
else:
    acc_phy_lib_elias.plot_default_msi_blade(twiss_no_mkp_q20, ax5, 'msi_test_1')
acc_phy_lib_elias.plot_aper_real(new_pos_x_q20[ind_q20], aper_neat_x_q20[ind_q20], ax5, offset=offset_x_q20[ind_q20])
plt.text(31.5, -0.09, 'n = {}'.format(n), fontsize=15, horizontalalignment='left')

fig6 = plt.figure(figsize=(10,7))
fig6.suptitle('SPS injection - Q26, dumped beam',fontsize=20)
ax6 = fig6.add_subplot(1, 1, 1)  # create an axes object in the figure
ax6.yaxis.label.set_size(20)
ax6.xaxis.label.set_size(20)
plt.xticks(fontsize=14)  #can also set rotation
plt.yticks(fontsize=14)
plt.ylim(-0.1, 0.15)
plt.xlim(30, 92)
acc_phy_lib_elias.plot_envelope(twiss_no_mkp_q26, env_q26.sige, env_q26.ex, env_q26.ey, ax6)
if env_q26.default_blade_flag == 0:
    acc_phy_lib_elias.plot_msi_blade(twiss_no_mkp_q26, 'msi_test_1', 'msi_test_4', ax6, dx=msi_blade_dx)
else:
    acc_phy_lib_elias.plot_default_msi_blade(twiss_no_mkp_q26, ax6, 'msi_test_1')
acc_phy_lib_elias.plot_aper_real(new_pos_x_q26[ind_q26], aper_neat_x_q26[ind_q26], ax6, offset=offset_x_q26[ind_q26])
plt.text(31.5, -0.09, 'n = {}'.format(n), fontsize=15, horizontalalignment='left')

fig4 = plt.figure(figsize=(10,7))
fig4.suptitle('SPS injection - SFTPRO dumped beam',fontsize=20)
ax4 = fig4.add_subplot(1, 1, 1)  # create an axes object in the figure
ax4.yaxis.label.set_size(20)
ax4.xaxis.label.set_size(20)
plt.xticks(fontsize=14)  #can also set rotation
plt.yticks(fontsize=14)
plt.ylim(-0.1, 0.15)
plt.xlim(30, 92)
acc_phy_lib_elias.plot_envelope(twiss_no_mkp_sftpro, env_sftpro.sige, env_sftpro.ex, env_sftpro.ey, ax4, nx=6, ny=4)
if env_sftpro.default_blade_flag == 0:
    acc_phy_lib_elias.plot_msi_blade(twiss_no_mkp_sftpro, 'msi_test_1', 'msi_test_4', ax4, dx=msi_blade_dx)
else:
    acc_phy_lib_elias.plot_default_msi_blade(twiss_no_mkp_sftpro, ax4, 'msi_test_1')
acc_phy_lib_elias.plot_aper_real(new_pos_x_sftpro[ind_sftpro], aper_neat_x_sftpro[ind_sftpro], ax4, offset=offset_x_sftpro[ind_sftpro])
plt.text(31.5, -0.09, 'n = {}'.format(n), fontsize=15, horizontalalignment='left')
#"""
