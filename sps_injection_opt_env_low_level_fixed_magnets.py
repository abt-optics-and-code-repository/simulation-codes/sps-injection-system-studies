"""
Low-level optimisation environment for a given SPS injection optics,
whose step function is optimised to find ideal strength of the MKP and MSI magnets, where 
magnet positions and lengths are fixed from previous iterations
"""

import numpy as np
import pandas as pd
from cpymad.madx import Madx

#Import accelerator physics library with functions
import acc_phy_lib_elias 

class OptEnv():
    
    def __init__(self, optics, n, mkp_nr, max_Bdl=None):
        
        #Choose optics and number of magnet modules
        self.optics = optics  
        self.mkp_nr =  mkp_nr
        self.n = n  #starting number of MKP magnet modules 
        self.msi_space = 0.4  #meters between each MSI magnet, keep fixed for now
        self.default_blade_flag = 0  #set to 0 for customised MSI blade when checking acceptance, otherwise set to 1 for default MSI blade 
        self.offset = 0. #-0.019 #horizontal aperture offset of last MKP, to facilitate for beam envelope to fit 
        self.offset_nr = 2  #number of MKPs, starting from the end, that have a small off-set
        self.msi_length = 2.0 #length in metres of MSI magnet 
        self.Bdl_org = 3.17200000e-01  #Original total Bdl values for all MKPs for Q20/Q26, calculated from notebook "today_sps_injection_acceptance_q20"
        self.Bdl_org_sftpro = 2.14273280e-01 #total Bdl for SFTPRO in today's values 
        if max_Bdl:  #if max Bdl threshold is given set it, otherwise set it to today's value 
            self.max_Bdl = max_Bdl #maximum Bdl, from previous runs 
        else:
            self.max_Bdl = self.Bdl_org
        self.reset()
    
    
    #One step in the optimisation, where the positions and lengths of magnets are fixed other optics and passed through parameter y
    #only optimiing for the MSI and MKP strengths stored in the free parameter vector x 
    def step(self, x, y):
        
        #Change the MSI and MKP kick
        self.madx.use(sequence='short_injection_basic')
        self.madx.globals['kmsi'] = x[0]  #MSI kick 
        self.madx.globals['kmkpa11931'] = x[1] #MKP kick  
        
        BRHO = self.madx.sequence['short_injection_basic'].beam.pc*3.3356
        
        #Move the magnets to the given positions
        self.madx.command.seqedit(sequence='short_injection_basic')
        self.madx.command.flatten()
        for k in range(self.mkp_nr):
            self.madx.input('mkp_module_{}, l:= 3.4230000000e+00*({}/5);'.format(k+1, self.n))
        self.madx.command.move(element='msi_test_1', to=y[0])
        self.madx.command.move(element='msi_test_2', to=y[0]+self.msi_space+self.msi_length)
        self.madx.command.move(element='msi_test_3', to=y[0]+2*self.msi_space+2*self.msi_length)
        self.madx.command.move(element='msi_test_4', to=y[0]+3*self.msi_space+3*self.msi_length)
        self.madx.command.move(element='mkp_module_1', to=y[1])
        for k in range(1, self.mkp_nr):  #Move the MKP modules to each location with proper spacing 
            y_mkp = np.sum(y[1:(2+k)])
            self.madx.command.move(element='mkp_module_{}'.format(k+1), to=y_mkp)
        self.madx.command.move(element='tbsj.11995', to=87.5)  #move the TBSJ as much as possible downstream
        self.madx.command.flatten()
        self.madx.command.endedit()  
    
        #Attempt Twiss command , restart MADX if it crashes 
        try:        
            #Perform Twiss command
            self.madx.use(sequence='short_injection_basic')
            twiss_return = self.madx.twiss(betx=self.twiss_beta1['betx'], alfx=-self.twiss_beta1['alfx'],
                                bety=self.twiss_beta1['bety'], alfy=-self.twiss_beta1['alfy'], 
                               dx=self.twiss_beta1['dx'], dpx=-self.twiss_beta1['dpx'],
                                dy = self.twiss_beta1['dy'], dpy=-self.twiss_beta1['dpy'],
                                x=self.twiss_beta1['x'], y=self.twiss_beta1['y'],
                                px=-self.twiss_beta1['px'], py=-self.twiss_beta1['py']).dframe()
           
            #Main penalty function for stable final beam in horizontal plane 
            objective_func = (twiss_return.x['qf.12010'])**2 + (twiss_return.px['qf.12010'])**2 
            
            #Also calculate Bdl from given MKP kick, penalise if higher than today's value 
            Bdl =  (self.mkp_nr*x[1])*BRHO
            objective_func += max(0, Bdl - self.max_Bdl)**2
            
            # -------------- Check aperture acceptance after the MSI, also with offset ------------------------------ 
            new_pos_x, aper_neat_x = acc_phy_lib_elias.get_apertures_real(twiss_return)
            new_pos_y, aper_neat_y = acc_phy_lib_elias.get_apertures_real(twiss_return, 2)
            offset_x = acc_phy_lib_elias.get_aper_offset_real(twiss_return)
            
            #Extract the twiss values without the start and end markers
            twiss_reduced = twiss_return.iloc[1:-2 , :]
            
            #Exclude the values before qda.11910
            ind = (new_pos_x >= twiss_reduced.loc['qda.11910'].s)
            A_x, A_y, val_x, idx_x, real_pos_x, val_y, idx_y, real_pos_y = acc_phy_lib_elias.get_smallest_acceptance_real(twiss_reduced, aper_neat_x, aper_neat_y, new_pos_x, new_pos_y, ind, self.ex, self.ey, offset_x=offset_x)
            
            #Set minimum values for acceptance vertically 
            if self.optics == 'sftpro':
                minimum_val_y = 4.67
            else:
                minimum_val_y = 6    
            
            #Add penalties if minimum acceptance is too low, in continous way
            objective_func += max(0, 6.0 - val_x)**2
            objective_func += max(0, minimum_val_y - val_y)**2
                
            #Check minimum acceptance of MSI blade, otherwise add penalty  
            A_x_min_msi, s_min = acc_phy_lib_elias.get_smallest_acceptance_real_MSI(twiss_reduced, self.ex, 'msi_test_1', 'msi_test_4', dx=0.02, default_blade_flag=self.default_blade_flag) #MSi blade with half thickness
            if self.optics == 'sftpro':  
                A_x_msi_req = 5.8
            else:
                A_x_msi_req = 7.5 
            
            #Add penalties if MSI acceptance low
            objective_func += max(0, A_x_msi_req - A_x_min_msi)**2
        
            # -------------- Check that acceptance at last MKP is large enough for dumped beam, such that beam can safely hit TBSJ ------------------------------ 
            self.madx.use(sequence='short_injection_basic')
            self.madx.globals['kmkpa11931'] = 0 #temporarily turn MKPs off 
            #initial condition examplation here: https://gitlab.cern.ch/abt-optics-and-code-repository/simulation-codes/cpymad_examples/-/blob/master/reflected_lattice_simple_example.ipynb 
            twiss_mkp = self.madx.twiss(betx=self.twiss_beta1['betx'], alfx=-self.twiss_beta1['alfx'],
                        bety=self.twiss_beta1['bety'], alfy=-self.twiss_beta1['alfy'], 
                       dx=self.twiss_beta1['dx'], dpx=-self.twiss_beta1['dpx'],
                        dy = self.twiss_beta1['dy'], dpy=-self.twiss_beta1['dpy'],
                        x=self.twiss_beta1['x'], y=self.twiss_beta1['y'],
                        px=-self.twiss_beta1['px'], py=-self.twiss_beta1['py']).dframe()
            
            #Enough to check acceptance at endpoint of MKP, the last element before the TBSJ
            #Give penalty if the beam hits before the TBSJ but also after 
            A_x_mkp = np.divide(abs(-twiss_mkp['aper_1'].loc['mkp_module_{}'.format(self.mkp_nr)] + self.offset)  - abs(twiss_mkp['x'].loc['mkp_module_{}'.format(self.mkp_nr)]), np.sqrt(twiss_mkp['betx'].loc['mkp_module_{}'.format(self.mkp_nr)]*self.ex))
            A_x_tbsj = np.divide(twiss_mkp['aper_1'].loc['tbsj.11995'] - abs(twiss_mkp['x'].loc['tbsj.11995']), np.sqrt(twiss_mkp['betx'].loc['tbsj.11995']*self.ex)) 
            self.A_x_mkp_dumped_beam = A_x_mkp  #save the value of minimum acceptance for dumped beam
            
            #Set the maximum (negative) value of TBSJ acceptance --> lower means further down on injection dump
            if self.optics == 'sftpro':
                min_A_tbsj = -4.6  #should be negative  
                min_A_mkp = 4.6  #should be positive
            else:
                min_A_tbsj = -5.0  #should be negative 
                min_A_mkp = 5.5 #should be positive
            
            #Add penalties if acceptance around TBSJ is too high or too low
            objective_func += max(0, min_A_mkp - A_x_mkp)**2
            objective_func += max(0, A_x_tbsj - min_A_tbsj)**2  #A_x_tbsj must be lower 
            
            #Reset the MKP kicks
            self.madx.use(sequence='short_injection_basic')
            self.madx.globals['kmkpa11931'] =x[1] #reset MKP kick  
        
        except RuntimeError:
            
            print("Resetting MADX...") 
            with open('tempfile', 'r') as f:
                lines = f.readlines()
                for ele in lines:
                    if '+=+=+= fatal' in ele:
                        print('{}'.format(ele))
            self.reset()
               
            #Set f to a high value
            objective_func = 100
                    
        return objective_func
    
    
    #Method to reset madx if it crashes
    def reset(self):
        
        #If MAD-X fails, re-spawn process   
        if 'self.madx' in globals():
            del self.madx
            
        with open('tempfile', 'w') as f:
            self.madx = Madx(stdout=f,stderr=f)
 
        self.madx.option(echo=False, warn=True, info=False, debug=False, verbose=False)
               
        #Reload the initial stripped sequence 
        self.twiss_reversed, self.twiss_forward_sps, self.survey_sps_forward, self.twiss_beta1, self.sige, self.ex, self.ey = self.load_stripped_seq()
            
        #Create new classes, with original length and kick to start with, to start optimizing 
        self.madx.input('msi_new: hkicker, l:= 2.0000000000e+00, kick:=-kmsi, apertype=rectangle, aperture={0.0645, 0.02425};')  
        self.madx.input('mkp_new: hkicker, kick:=kmkpa11931, l:= 3.4230000000e+00*({}/5), apertype=rectangle, aperture={{0.05,0.0305}};'.format(self.n))  #based on the mkp-a type kicker with MKP-A aperture
        self.madx.input('mkp_offset: hkicker, kick:=kmkpa11931, l:= 3.4230000000e+00*({}/5), apertype=rectangle, aperture={{0.05,0.0305}}, aper_offset={{{},0.0}};'.format(self.n, self.offset))
        
        #For Q26 and SFTPRO: manually add the apertures at the end of the line, as the aperture file for some reason is not registred 
        if self.optics != 'q20':
            self.madx.input('qif.103000, APERTYPE=ELLIPSE, APERTURE={0.076,0.01915};')
            self.madx.input('qda.11910, APERTYPE=ELLIPSE, APERTURE={0.0755,0.0455};')
            self.madx.input('tbsj.11995, APERTYPE=RECTANGLE, APERTURE={0.0608,0.016};')
            self.madx.input('qf.12010 , APERTYPE=ELLIPSE, APERTURE={0.076,0.01915};')
        
        #Place new magnets
        self.madx.command.seqedit(sequence='short_injection_basic')
        self.madx.command.flatten()
        self.madx.command.install(element='msi_test_1', class_='msi_new', at=35.0)
        self.madx.command.install(element='msi_test_2', class_='msi_new', at=37.4)
        self.madx.command.install(element='msi_test_3', class_='msi_new', at=39.8)
        self.madx.command.install(element='msi_test_4', class_='msi_new', at=42.2)
        self.madx.command.install(element='mkp_module_1', class_='mkp_new', at=67.0)
        
        #Find minimum distance between initial MKP tanks
        self.mkp_org_l = 3.4230000000e+00
        self.s_min_mkp = self.mkp_org_l/2*self.n/5 + self.mkp_org_l/2*self.n/5 + 0.3  #minimum MKP tank placement
    
        #Install the modules at a preliminary location (s = 67 m), except the very last one with a small off-set
        for k in range(1, self.mkp_nr):  
            if k >= self.mkp_nr - self.offset_nr:
                self.madx.command.install(element='mkp_module_{}'.format(k+1), class_='mkp_offset', at=67.0+k*self.s_min_mkp)
            else: 
                self.madx.command.install(element='mkp_module_{}'.format(k+1), class_='mkp_new', at=67.0+k*self.s_min_mkp)
            self.madx.input('mkp_module_{}, l:= 3.4230000000e+00*({}/5);'.format(k+1, self.n))
        
        self.madx.command.flatten()
        self.madx.command.endedit()

        
    #Load the initial stripped SPS injection sequence 
    def load_stripped_seq(self):
        
        #Call the short SPS sequence file
        self.madx.call("initial_twisses_and_sequences/sps_short_injection_basic_{}.seq".format(self.optics))
    
        #Load Twiss and survey parameters from today's sequence 
        twiss_reversed = pd.read_csv("initial_twisses_and_sequences/sps_injection_twiss_reversed_{}.csv".format(self.optics))
        twiss_forward_sps = pd.read_csv("initial_twisses_and_sequences/sps_injection_twiss_forward_{}.csv".format(self.optics))
        survey_sps_forward = pd.read_csv("initial_twisses_and_sequences/survey_sps_forward_{}.csv".format(self.optics))
        twiss_beta1 = twiss_reversed.iloc[-1] #locate Twiss parameters at the start of the loaded reversed sequence, initial conditions for later
        
        #Call the old aperture files for SPS 
        self.madx.call("sps_aperture_data/aperturedb_1.dbx")
        self.madx.call("sps_aperture_data/aperturedb_2.dbx")
        self.madx.call("sps_aperture_data/aperturedb_3.dbx")
    
        #Activate the aperture for the Twiss flag to include it in Twiss command! 
        self.madx.input('select,flag=twiss,clear;')
        self.madx.select(flag='twiss', column=['N1', 'apertype', 'aper_1', 'aper_2', 'aper_3', 'aper_4', 'apoff_1', 'apoff_2', 'aptol_1', 'aptol_2', 'aptol_3'])
        
        #Extract beam info
        self.madx.use(sequence='short_injection_basic')
        sige = self.madx.sequence['short_injection_basic'].beam['sige']  #relative energy spread
        ex = self.madx.sequence['short_injection_basic'].beam['ex']
        ey = self.madx.sequence['short_injection_basic'].beam['ey']
    
        #Strip sequence of all free parameters 
        self.madx.command.seqedit(sequence='short_injection_basic')
        self.madx.command.flatten()
        self.madx.command.remove(element='mdca.103004')
        self.madx.command.remove(element='msi.118350')
        self.madx.command.remove(element='msi.118380')
        self.madx.command.remove(element='msi.118520')
        self.madx.command.remove(element='msi.118550')
        self.madx.command.remove(element='mdva.11904')
        self.madx.command.remove(element='mkpa.11931')
        self.madx.command.remove(element='mkpa.11936')
        self.madx.command.remove(element='mkpc.11952')
        self.madx.command.remove(element='mkp.11955')
        self.madx.command.remove(element='mdsh.11971') 
        self.madx.command.remove(element='bph.12008')
        self.madx.command.flatten()
        self.madx.command.endedit()
        
        return twiss_reversed, twiss_forward_sps, survey_sps_forward, twiss_beta1, sige, ex, ey

    #Method to return Twiss, both for normal and dumped beam (without the MKP kick)
    def return_twisses(self):
        
        #Store initial MKP kick
        mkp_kick_org = self.madx.globals['kmkpa11931']
        
        try:        
            #Perform Twiss command
            self.madx.use(sequence='short_injection_basic')
            twiss = self.madx.twiss(betx=self.twiss_beta1['betx'], alfx=-self.twiss_beta1['alfx'],
                                bety=self.twiss_beta1['bety'], alfy=-self.twiss_beta1['alfy'], 
                               dx=self.twiss_beta1['dx'], dpx=-self.twiss_beta1['dpx'],
                                dy = self.twiss_beta1['dy'], dpy=-self.twiss_beta1['dpy'],
                                x=self.twiss_beta1['x'], y=self.twiss_beta1['y'],
                                px=-self.twiss_beta1['px'], py=-self.twiss_beta1['py']).dframe()
            
            #Extract the twiss values without the start and end markers
            twiss_reduced = twiss.iloc[1:-2 , :]
            
            # -------------- Check minimum aperture acceptances, also for TBSJ ------------------------------ 
            new_pos_x, aper_neat_x = acc_phy_lib_elias.get_apertures_real(twiss_reduced)
            new_pos_y, aper_neat_y = acc_phy_lib_elias.get_apertures_real(twiss_reduced, 2)
            offset_x = acc_phy_lib_elias.get_aper_offset_real(twiss_reduced)
            
            #Find the acceptances, exclude the values before and including the MSI
            ind = (new_pos_x > twiss_reduced.loc['msi_test_4'].s)
            A_x, A_y, val_x, idx_x, real_pos_x, val_y, idx_y, real_pos_y = acc_phy_lib_elias.get_smallest_acceptance_real(twiss_reduced, aper_neat_x, aper_neat_y, new_pos_x, new_pos_y, ind, self.ex, self.ey, offset_x=offset_x)
                            
            #Check minimum acceptance of MSI blade, otherwise add penalty  
            A_x_min_msi, s_min = acc_phy_lib_elias.get_smallest_acceptance_real_MSI(twiss_reduced, self.ex, 'msi_test_1', 'msi_test_4', dx=0.02, default_blade_flag=self.default_blade_flag) #MSi blade with half thickness
            
            if A_x_min_msi < val_x:      #Keep the lowest of all acceptances 
                A_x_min_tot = A_x_min_msi #smallest acceptance at MSI
                s_min_tot = s_min 
            else:
                A_x_min_tot = val_x  #smallest acceptance at other parts of aperture
                s_min_tot = real_pos_x[idx_x] 
        
            # -------------- Check that acceptance at last MKP is large enough for dumped beam, such that beam can safely hit TBSJ ------------------------------ 
            self.madx.use(sequence='short_injection_basic')
            self.madx.globals['kmkpa11931'] = 0 #temporarily turn MKPs off 
            twiss_no_mkp = self.madx.twiss(betx=self.twiss_beta1['betx'], alfx=-self.twiss_beta1['alfx'],
                        bety=self.twiss_beta1['bety'], alfy=-self.twiss_beta1['alfy'], 
                       dx=self.twiss_beta1['dx'], dpx=-self.twiss_beta1['dpx'],
                        dy = self.twiss_beta1['dy'], dpy=-self.twiss_beta1['dpy'],
                        x=self.twiss_beta1['x'], y=self.twiss_beta1['y'],
                        px=-self.twiss_beta1['px'], py=-self.twiss_beta1['py']).dframe()
            
            #Reset the MKP kick
            self.madx.use(sequence='short_injection_basic')
            self.madx.globals['kmkpa11931'] = mkp_kick_org  #reset MKP kick  
            
        except RuntimeError:
            
            print("Resetting MADX...") 
            with open('tempfile_kicker', 'r') as f:
                lines = f.readlines()
                for ele in lines:
                    if '+=+=+= fatal' in ele:
                        print('{}'.format(ele))
            self.reset()
            
        return twiss, twiss_no_mkp, A_x_min_tot, s_min_tot, val_y, real_pos_y[idx_y]