"""
Script to run a given combination of MKP modules and tanks, initiating a global environment which optimises the low-level environments
This module is more to test the global environment for a given magnet configuration, the actual high-level optimisation happens in runOpt_sps_injection_meta_optimisation_pymoo
"""
import numpy as np
#matplotlib.use('Agg')  #to save plots remotely from ssh
import matplotlib.pyplot as plt
        
#Import high-level environment
import sps_injection_opt_env_global



#Initiate global environment 
glob_env = sps_injection_opt_env_global.OptEnv_global()

#Uncomment to test one iteration of step function
#"""
n = [4, 4]  #number of modules and tanks
n_modules = n[0]
glob_env.step(n) #number of modules per tank, and number of tanks
glob_env.optimise_kicks(n)

#Set flag to 1 if figures are to be saved
fig_flag = 0


# --------------------- PLOT ENVELOPE AND APERTURE IF NEEDED, TO SEE PERFORMANCE -------------------------------- 
fig1 = plt.figure(figsize=(10,7))
glob_env.plot_injection_q20(fig1)
if fig_flag == 1:
    fig1.savefig('plots/sps_injection_x_meta_{}_{}_n{}.png'.format('q20', glob_env.mkp_str, n_modules), dpi=400)

fig2 = plt.figure(figsize=(10,7))
glob_env.plot_injection_q26(fig2)
if fig_flag == 1:
    fig2.savefig('plots/sps_injection_x_meta_{}_{}_n{}.png'.format('q26', glob_env.mkp_str, n_modules), dpi=400)

fig3 = plt.figure(figsize=(10,7))
glob_env.plot_injection_sftpro(fig3)
if fig_flag == 1:
    fig3.savefig('plots/sps_injection_x_meta_{}_{}_n{}.png'.format('sftpro', glob_env.mkp_str, n_modules), dpi=400)

fig4 = plt.figure(figsize=(10,7))
glob_env.plot_injection_q20_no_mkp(fig4)
if fig_flag == 1:
    fig4.savefig('plots/sps_injection_x_meta_{}_{}_n{}_no_mkp.png'.format('q20', glob_env.mkp_str, n_modules), dpi=400)

fig5 = plt.figure(figsize=(10,7))
glob_env.plot_injection_q26_no_mkp(fig5)
if fig_flag == 1:
    fig5.savefig('plots/sps_injection_x_meta_{}_{}_n{}_no_mkp.png'.format('q26', glob_env.mkp_str, n_modules), dpi=400)

fig6 = plt.figure(figsize=(10,7))
glob_env.plot_injection_sftpro_no_mkp(fig6)
if fig_flag == 1:
    fig6.savefig('plots/sps_injection_x_meta_{}_{}_n{}_no_mkp.png'.format('sftpro', glob_env.mkp_str, n_modules), dpi=400)